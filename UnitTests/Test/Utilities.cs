﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Test
{
    [TestClass]
    public class Utilities
    {
        [TestMethod]
        public void ContarDivisoresDeSeis()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Numero = 6;

            //Act
            var Result = utilities.ContarDivisores(Numero);

            //Assert
            Assert.AreEqual(3, Result);

        }

        [TestMethod]
        public void ObtenerValorMouse()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Product = 1290;

            //Act
            var Result = utilities.ObtenerValorProducto(Product);

            //Assert
            Assert.AreEqual(280000, Result);
        }

        [TestMethod]
        public void SubTotal()
        {
            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Cantidad = 2;
            var Valor = 280000;

            //Act
            var Subtotal = utilities.CalcularSubtotal(Cantidad,Valor);
           

            //Assert
            Assert.AreEqual(560000, Subtotal);
        }

        [TestMethod]
        public void Descuento()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var subtotal = 500000;

            //Act
            var Descuento = utilities.CalcularDescuento(subtotal);


            //Assert
            Assert.AreEqual(0,Descuento);
        }

        [TestMethod]
        public void IVA()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Subtotal = 500000;

            //Act
            var IVA = utilities.CalcularIVA(Subtotal);

            //Assert
            Assert.AreEqual(95000, IVA);
        }

        [TestMethod]
        public void Total()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Subtotal = 400000;            

            //Act
            var Total = utilities.CalcularTotal(Subtotal);

            //Assert
            Assert.AreEqual(400000, Total);
        }
    }
}
